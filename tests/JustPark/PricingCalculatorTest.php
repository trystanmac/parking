<?php

namespace JustPark\Test;

use Carbon\Carbon;
use Illuminate\Container\Container;
use JustPark\PricingCalculator;

class PricingCalculatorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Pricing calculator instance.
     *
     * @var JustPark\PricingCalculator
     */
    private $calculator;

    /**
     * Instantiate the PricingCalculator class using the Laravel IoC container.
     *
     * This container will allow for automatic dependency injection based upon
     * constructor type hinting. See the Laravel documentation at
     *
     *  http://laravel.com/docs/ioc
     *
     * for more information.
     */
    public function setUp()
    {
        // Create a new Laravel container instance.
        $container = new Container;

        // Resolve the pricing calculator (and any type hinted dependencies)
        // and set to class attribute.
        $this->calculator = $container->make(PricingCalculator::class);
    }

    /**
     * Ensure that an empty array of time periods returns zero.
     */
    public function testEmptyArrayOfPeriodsReturnsZero()
    {
        $result = $this->calculator->calculate([]);
        $this->assertEquals(0, $result);
    }

    /**
     *  testCaseOne
     *
     *  | #   | Start           | End             | Duration  | Cost  | Notes                |
     *  | --- | --------------- | --------------- | --------- | ----- | -------------------- |
     *  | 1   | 24th Jan, 14:00 | 25th Jan, 03:00 | 1 day     | £5    | Finishes before 5am. |
     */
    public function testCaseOne()
    {
        $expectedCost = 5;

        $from = Carbon::now()
                ->setDate(2018, 1, 24)
                ->setTime(14, 0, 0);

        $to = Carbon::now()
                ->setDate(2018, 1, 25)
                ->setTime(3, 0, 0);

        $cost = $this->calculator->calculate([
            [$from, $to],
        ]);

        $this->assertEquals($expectedCost, $cost);
    }

    /**
     *  testCaseTwo
     *
     *  | #   | Start           | End             | Duration  | Cost  | Notes                                                              |
     *  | --- | --------------- | --------------- | --------- | ----- | ------------------------------------------------------------------ |
     *  | 2   | 24th Jan, 14:00 | 25th Jan, 12:00 | 2 days    | £10   | Finishes after 5am, so 2 days despite it being less than 24 hours. |
     */
    public function testCaseTwo()
    {
        $expectedCost = 10;

        $from = Carbon::now()
                ->setDate(2018, 1, 24)
                ->setTime(14, 0, 0);

        $to = Carbon::now()
                ->setDate(2018, 1, 25)
                ->setTime(12, 0, 0);

        $cost = $this->calculator->calculate([
            [$from, $to],
        ]);

        $this->assertEquals($expectedCost, $cost);
    }

    /**
     *  testCaseThree
     *
     *  | #   | Start           | End             | Duration           | Cost  | Notes                                                                                     |
     *  | --- | --------------- | --------------- | ------------------ | ----- | ----------------------------------------------------------------------------------------- |
     *  | 3   | 24th Jan, 14:00 | 18th Feb, 15:00 | 3 weeks and 5 days | £70   | Monthly price (£70) is less than (3 (weeks) * £20 + 5 (days) * £5) and (4 (weeks) * £20). |
     */
    public function testCaseThree()
    {
        $expectedCost = 70;

        $from = Carbon::now()
                ->setDate(2018, 1, 24)
                ->setTime(14, 0, 0);

        $to = Carbon::now()
                ->setDate(2018, 2, 18)
                ->setTime(15, 0, 0);

        $cost = $this->calculator->calculate([
            [$from, $to],
        ]);

        $this->assertEquals($expectedCost, $cost);
    }

    /**
     * testMultilplSessions
     * 
     *  | #   | Start           | End             | Duration           | Cost  | Notes                                                                                     |
     *  | --- | --------------- | --------------- | ------------------ | ----- | ----------------------------------------------------------------------------------------- |
     *  | 1   | 24th Jan, 14:00 | 25th Jan, 03:00 | 1 day              | £5    | Finishes before 5am.                                                                      |
     *  | 2   | 24th Jan, 14:00 | 25th Jan, 12:00 | 2 days             | £10   | Finishes after 5am, so 2 days despite it being less than 24 hours.                        |
     *  | 3   | 24th Jan, 14:00 | 18th Feb, 15:00 | 3 weeks and 5 days | £70   | Monthly price (£70) is less than (3 (weeks) * £20 + 5 (days) * £5) and (4 (weeks) * £20). |
     */
    public function testMultilplSessions()
    {
        $expectedCost = 5 + 10 + 70;

        // Session set 1
        $from1 = Carbon::now()
                ->setDate(2018, 1, 24)
                ->setTime(14, 0, 0);

        $to1 = Carbon::now()
                ->setDate(2018, 1, 25)
                ->setTime(3, 0, 0);

        // Session set 2
        $from2 = Carbon::now()
                ->setDate(2018, 1, 24)
                ->setTime(14, 0, 0);

        $to2 = Carbon::now()
                ->setDate(2018, 1, 25)
                ->setTime(12, 0, 0);

        // Session set 3
        $from3 = Carbon::now()
                ->setDate(2018, 1, 24)
                ->setTime(14, 0, 0);

        $to3 = Carbon::now()
                ->setDate(2018, 2, 18)
                ->setTime(15, 0, 0);

        $sessions = [
            [$from1, $to1],
            [$from2, $to2],
            [$from3, $to3],
        ];

        $cost = $this->calculator->calculate($sessions);

        $this->assertEquals($expectedCost, $cost);
    }
}
