<?php

namespace JustPark\Test\Duration;

use Carbon\Carbon;
use JustPark\Duration\Hours;
use JustPark\DateRangeSetterInterface;
use JustPark\Duration\DurationInterface;

class HoursTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Pricing Hours instance.
     *
     * @var JustPark\Pricing\Hours
     */
    private $objectUnderTest;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->objectUnderTest = new Hours;
    }

    /**
     * testImplementsDurationInterface
     */
    public function testImplementsDurationInterface()
    {
        $this->assertInstanceOf(
            DurationInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testImplementsDateRangeSetterInterface
     */
    public function testImplementsDateRangeSetterInterface()
    {
        $this->assertInstanceOf(
            DateRangeSetterInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testSetDurationTwoHours
     */
    public function testSetDurationMoreThanTwoHours()
    {
        $mins = 60 + 60 + 15;
        $expectedHourlyDuration = 3;
        $from = Carbon::now();
        $to = Carbon::now()->addMinutes($mins);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedHourlyDuration,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testSetDurationLessThanOneHourReturnsOneHour
     */
    public function testSetDurationLessThanOneHourReturnsOneHour()
    {
        $mins = 10;
        $expectedHourlyDuration = 1;
        $from = Carbon::now();
        $to = Carbon::now()->addMinutes($mins);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedHourlyDuration,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testSetDurationSameEqualsZeroHours
     */
    public function testSetDurationSameEqualsZeroHours()
    {
        $expectedHourlyDuration = 0;
        $from = Carbon::now();
        $to = Carbon::now();

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedHourlyDuration,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testDurationWithoutCallingSetDateRange
     * 
     * @expectedException Exception
     * @expectedExceptionMessage Error, DateRange not set.
     */
    public function testDurationWithoutCallingSetDateRange()
    {
        $this->objectUnderTest->duration();
    }

    /**
     * testSetDateRangeFromAfterTo
     * 
     * @expectedException Exception
     * @expectedExceptionMessage Error, End date should be after Start date.
     */
    public function testSetDateRangeFromAfterTo()
    {
        $from = Carbon::now()->tomorrow();
        $to = Carbon::now();

        $this->objectUnderTest->setDateRange($from, $to);
    }
}
