<?php

namespace JustPark\Test\Duration;

use Carbon\Carbon;
use JustPark\Duration\Months;
use JustPark\DateRangeSetterInterface;
use JustPark\Duration\DurationInterface;

class MonthsTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Duration Months instance.
     *
     * @var JustPark\Duration\Months
     */
    private $objectUnderTest;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->objectUnderTest = new Months;
    }

    /**
     * testImplementsDurationInterface
     */
    public function testImplementsDurationInterface()
    {
        $this->assertInstanceOf(
            DurationInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testImplementsDateRangeSetterInterface
     */
    public function testImplementsDateRangeSetterInterface()
    {
        $this->assertInstanceOf(
            DateRangeSetterInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testSetDateRange
     */
    public function testSetDateRange()
    {
        $from = Carbon::now();
        $to = Carbon::now();

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setDateRange($from, $to)
        );
    }

    /**
     * testDurationLessThanOneMonth
     */
    public function testDurationLessThanOneMonth()
    {
        $expectedNumberOfMonths = 1;
        $weeks = 3;
        $from = Carbon::now();
        $to = Carbon::now()->addWeeks($weeks);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedNumberOfMonths,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testDurationMoreThanOneMonth
     */
    public function testDurationMoreThanOneMonth()
    {
        $expectedNumberOfMonths = 2;
        $weeks = 6;
        $from = Carbon::now();
        $to = Carbon::now()->addWeeks($weeks);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedNumberOfMonths,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testDurationWithoutCallingSetDateRange
     * 
     * @expectedException Exception
     * @expectedExceptionMessage Error, DateRange not set.
     */
    public function testDurationWithoutCallingSetDateRange()
    {
        $this->objectUnderTest->duration();
    }

    /**
     * testSetDateRangeFromAfterTo
     * 
     * @expectedException Exception
     * @expectedExceptionMessage Error, End date should be after Start date.
     */
    public function testSetDateRangeFromAfterTo()
    {
        $from = Carbon::now()->tomorrow();
        $to = Carbon::now();

        $this->objectUnderTest->setDateRange($from, $to);
    }
}
