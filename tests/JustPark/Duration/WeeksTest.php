<?php

namespace JustPark\Test\Duration;

use Carbon\Carbon;
use JustPark\Duration\Weeks;
use JustPark\DateRangeSetterInterface;
use JustPark\Duration\DurationInterface;

class WeeksTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Pricing Days instance.
     *
     * @var JustPark\Pricing\Days
     */
    private $objectUnderTest;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->objectUnderTest = new Weeks;
    }

    /**
     * testImplementsDurationInterface
     */
    public function testImplementsDurationInterface()
    {
        $this->assertInstanceOf(
            DurationInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testImplementsDateRangeSetterInterface
     */
    public function testImplementsDateRangeSetterInterface()
    {
        $this->assertInstanceOf(
            DateRangeSetterInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testSetDateRange
     */
    public function testSetDateRange()
    {
        $duration = 1;
        $from = Carbon::now();
        $to = Carbon::now()->addDays($duration);

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setDateRange($from, $to)
        );
    }

    /**
     * testDurationLessThanOneWeek
     */
    public function testDurationLessThanOneWeek()
    {
        $expectedNumberOfWeeks = 1;
        $days = 6;
        $from = Carbon::now();
        $to = Carbon::now()->addDays($days);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedNumberOfWeeks,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testDurationTwoHoursDifference
     */
    public function testDurationTwoHoursDifference()
    {
        $expectedNumberOfWeeks = 1;
        $hours = 2;
        $from = Carbon::now();
        $to = Carbon::now()->addHours($hours);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedNumberOfWeeks,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testDurationMoreThanOneWeek
     */
    public function testDurationMoreThanOneWeek()
    {
        $expectedNumberOfWeeks = 2;
        $days = 8;
        $from = Carbon::now();
        $to = Carbon::now()->addDays($days);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedNumberOfWeeks,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testDurationMoreThanSevenWeeks
     */
    public function testDurationMoreThanSevenWeeks()
    {
        $expectedNumberOfWeeks = 8;
        
        // Note:(7 days * 6 weeks) + 1 day
        // This means that we are in the eight week.
        $days = (7 * 7) + 1;
        $from = Carbon::now();
        $to = Carbon::now()->addDays($days);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedNumberOfWeeks,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testSetDateRangeFromAfterTo
     * 
     * @expectedException Exception
     * @expectedExceptionMessage Error, End date should be after Start date.
     */
    public function testSetDateRangeFromAfterTo()
    {
        $from = Carbon::now()->tomorrow();
        $to = Carbon::now();

        $this->objectUnderTest->setDateRange($from, $to);
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Error, DateRange not set.
     */
    public function testDurationWithoutCallingSetDateRange()
    {
        $this->objectUnderTest->duration();
    }
}
