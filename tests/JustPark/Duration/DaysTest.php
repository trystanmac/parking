<?php

namespace JustPark\Test\Duration;

use Carbon\Carbon;
use JustPark\Duration\Days;
use JustPark\DateRangeSetterInterface;
use JustPark\Duration\DurationInterface;

class DaysTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Duration Days instance.
     *
     * @var JustPark\Duration\Days
     */
    private $objectUnderTest;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->objectUnderTest = new Days;
    }

    /**
     * testImplementsDurationInterface
     */
    public function testImplementsDurationInterface()
    {
        $this->assertInstanceOf(
            DurationInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testImplementsDateRangeSetterInterface
     */
    public function testImplementsDateRangeSetterInterface()
    {
        $this->assertInstanceOf(
            DateRangeSetterInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testSetDateRange
     */
    public function testSetDateRange()
    {
        $duration = 1;
        $from = Carbon::now();
        $to = Carbon::now()->addDays($duration);

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setDateRange($from, $to)
        );
    }

    /**
     * testDurationLessThanOneDayExpectMinimumDurationToBeOneDaySpanTwoDays
     *
     * This logic is to counteract the "Last day not included if
     * it ends before five am".
     *
     * This tests tests the time span going into the next day.
     */
    public function testDurationLessThanOneDayExpectMinimumDurationToBeOneDaySpanTwoDays()
    {
        $expectedNumberOfDays = 1;
        
        $from = Carbon::now()
                ->setTime(23, 00, 00);

        $hours = 2;
        $to = Carbon::now()
              ->setTime(23, 00, 00)
              ->addHours($hours);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedNumberOfDays,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testDurationLessThanOneDayExpectMinimumDurationToBeOneDay
     */
    public function testDurationLessThanOneDayExpectMinimumDurationToBeOneDay()
    {
        $expectedNumberOfDays = 1;
        $hours = 2;
        $from = Carbon::now()
                ->setTime(11, 00, 00);

        $to = Carbon::now()
              ->setTime(11, 00, 00)
              ->addHours($hours);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedNumberOfDays,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testDurationMoreThanTwoDays
     */
    public function testDurationMoreThanTwoDays()
    {
        $expectedDays = 3;
        $hours = 24 + 24 + 12;

        $from = Carbon::now()
                ->setTime(7, 0, 0);

        $to = Carbon::now()
                ->setTime(7, 0, 0)
                ->addHours($hours);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedDays,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testOneDayAsDayTwoBeforeFiveAm
     */
    public function testOneDayAsDayTwoBeforeFiveAm()
    {
        $expectedDays = 1;

        $from = Carbon::now()
                ->setDate(2018, 1, 24)
                ->setTime(14, 0, 0);

        $to = Carbon::now()
                ->setDate(2018, 1, 25)
                ->setTime(4, 59, 59);

        $this->objectUnderTest->setDateRange($from, $to);

        $this->assertEquals(
            $expectedDays,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testTwoDaysAsDayTwoAfterFiveAm
     */
    public function testTwoDaysAsDayTwoAfterFiveAm()
    {
        $expectedDays = 2;

        $from = Carbon::now()
                ->setDate(2018, 1, 24)
                ->setTime(14, 0, 0);

        $to = Carbon::now()
                ->setDate(2018, 1, 25)
                ->setTime(5, 00, 01);

        $this->objectUnderTest->setDateRange($from, $to);
        
        $this->assertEquals(
            $expectedDays,
            $this->objectUnderTest->duration()
        );
    }

    /**
     * testDurationWithoutCallingSetDateRange
     * 
     * @expectedException Exception
     * @expectedExceptionMessage Error, DateRange not set.
     */
    public function testDurationWithoutCallingSetDateRange()
    {
        $this->objectUnderTest->duration();
    }

    /**
     * testSetDateRangeFromAfterTo
     * 
     * @expectedException Exception
     * @expectedExceptionMessage Error, End date should be after Start date.
     */
    public function testSetDateRangeFromAfterTo()
    {
        $from = Carbon::now()->tomorrow();
        $to = Carbon::now();

        $this->objectUnderTest->setDateRange($from, $to);
    }
}
