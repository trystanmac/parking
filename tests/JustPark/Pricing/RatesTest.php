<?php

namespace JustPark\Test\Pricing;

use Illuminate\Container\Container;

use JustPark\Pricing\Days;
use JustPark\Pricing\Hours;
use JustPark\Pricing\Months;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\Pricing\Rates;
use JustPark\Pricing\Weeks;

class RatesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Pricing instance.
     *
     * @var JustPark\Pricing\Rates
     */
    private $objectUnderTest;

    /** @var JustPark\Pricing\Hours */
    private $hoursMock;

    /**
     * Rates
     */
    public function setUp()
    {
        $this->objectUnderTest = new Rates;
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Error, no rates available for invalid class 'JustPark\Pricing\Months'
     */
    public function testGetRateWithInvalidClass()
    {
        $container = new Container;
        $months = $container->make(Months::class);
        $this->objectUnderTest->rate($months);
    }

    /**
     * testAddReturn
     */
    public function testAddReturn()
    {
        $rateDays = 30;
        
        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->add(
                $this->createMock(Days::class),
                $rateDays
            )
        );
    }

    /**
     * testAddAndGet
     */
    public function testAddAndGet()
    {
        $rateMonths = 29;
        $this->objectUnderTest->add(
            $this->createMock(Months::class),
            $rateMonths
        );


        $rateDays = 30;
        $this->objectUnderTest->add(
            $this->createMock(Days::class),
            $rateDays
        );

        $this->assertEquals(
            $rateMonths,
            $this->objectUnderTest->rate($this->createMock(Months::class))
        );

        $this->assertEquals(
            $rateDays,
            $this->objectUnderTest->rate($this->createMock(Days::class))
        );
    }
}
