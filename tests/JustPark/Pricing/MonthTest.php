<?php

namespace JustPark\Test\Pricing;

use Carbon\Carbon;
use JustPark\Duration\Months as DurationMonths;
use JustPark\Pricing\Weeks as PricingWeeks;
use JustPark\Pricing\Months;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\DateRangeSetterInterface;
use JustPark\Pricing\Rates;

class MonthsTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Pricing Days instance.
     *
     * @var JustPark\Pricing\Days
     */
    private $objectUnderTest;

    /** @var Rates */
    private $ratesMock;

    /** @var DurationWeeks */
    private $durationMonthsMock;

    /** @var PricingDays */
    private $pricingWeeksMock;

    /** @var Carbon */
    private $carbonFromMock;

    /** @var Carbon */
    private $carbonToMock;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->ratesMock = $this->createMock(Rates::class);
        $this->durationMonthsMock = $this->createMock(DurationMonths::class);
        $this->pricingWeeksMock = $this->createMock(PricingWeeks::class);

        $this->carbonFromMock = $this->createMock(Carbon::class);
        $this->carbonToMock = $this->createMock(Carbon::class);
        
        $this->objectUnderTest = new Months(
                $this->ratesMock,
                $this->durationMonthsMock,
                $this->pricingWeeksMock
        );
    }

    /**
     * testImplementsPriceCalcInterface
     */
    public function testImplementsPriceCalcInterface()
    {
        $this->assertInstanceOf(
            PriceCalcInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testImplementsDateRangeSetterInterface
     */
    public function testImplementsDateRangeSetterInterface()
    {
        $this->assertInstanceOf(
            DateRangeSetterInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testSetDateRange
     */
    public function testSetDateRange()
    {
        $this
            ->durationMonthsMock
            ->expects($this->once())
            ->method('setDateRange')
            ->with(
                $this->identicalTo($this->carbonFromMock),
                $this->identicalTo($this->carbonToMock)
            );

        $this
            ->pricingWeeksMock
            ->expects($this->once())
            ->method('setDateRange')
            ->with(
                $this->identicalTo($this->carbonFromMock),
                $this->identicalTo($this->carbonToMock)
            );

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setDateRange($this->carbonFromMock, $this->carbonToMock)
        );
    }

    /**
     * testPriceReturnMonthsPriceAsIsLower
     */
    public function testPriceReturnMonthsPriceAsIsLower()
    {
        $duration = 2;
        $rate = 8;
        $monthPrice = $duration * $rate;
        $weekPrice = $monthPrice + 1;

        $this
            ->pricingWeeksMock
            ->expects($this->once())
            ->method('price')
            ->willReturn($weekPrice);

        $this
            ->durationMonthsMock
            ->expects($this->once())
            ->method('duration')
            ->willReturn($duration);
        
        $this
            ->ratesMock
            ->expects($this->once())
            ->method('rate')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn($rate);

        $this->assertEquals(
            $monthPrice,
            $this->objectUnderTest->price()
        );
    }

    /**
     * testPriceReturnWeeksPriceIsLower
     */
    public function testPriceReturnWeeksPriceIsLower()
    {
        $duration = 2;
        $rate = 8;
        $monthPrice = $duration * $rate;
        $weekPrice = $monthPrice - 1;

        $this
            ->pricingWeeksMock
            ->expects($this->once())
            ->method('price')
            ->willReturn($weekPrice);

        $this
            ->durationMonthsMock
            ->expects($this->once())
            ->method('duration')
            ->willReturn($duration);
        
        $this
            ->ratesMock
            ->expects($this->once())
            ->method('rate')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn($rate);

        $this->assertEquals(
            $weekPrice,
            $this->objectUnderTest->price()
        );
    }
}
