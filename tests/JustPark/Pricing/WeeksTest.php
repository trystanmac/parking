<?php

namespace JustPark\Test\Pricing;

use Carbon\Carbon;
use JustPark\Duration\Weeks as DurationWeeks;
use JustPark\Pricing\Days as PricingDays;
use JustPark\Pricing\Weeks;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\DateRangeSetterInterface;
use JustPark\Pricing\Rates;

class WeeksTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Pricing Days instance.
     *
     * @var JustPark\Pricing\Days
     */
    private $objectUnderTest;

    /** @var Rates */
    private $ratesMock;

    /** @var DurationWeeks */
    private $durationWeeksMock;

    /** @var PricingDays */
    private $pricingDaysMock;

    /** @var Carbon */
    private $carbonFromMock;

    /** @var Carbon */
    private $carbonToMock;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->ratesMock = $this->createMock(Rates::class);
        $this->durationWeeksMock = $this->createMock(DurationWeeks::class);
        $this->pricingDaysMock = $this->createMock(PricingDays::class);

        $this->carbonFromMock = $this->createMock(Carbon::class);
        $this->carbonToMock = $this->createMock(Carbon::class);
        
        $this->objectUnderTest = new Weeks(
                $this->ratesMock,
                $this->durationWeeksMock,
                $this->pricingDaysMock
        );
    }

    /**
     * testImplementsPriceCalcInterface
     */
    public function testImplementsPriceCalcInterface()
    {
        $this->assertInstanceOf(
            PriceCalcInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testImplementsDateRangeSetterInterface
     */
    public function testImplementsDateRangeSetterInterface()
    {
        $this->assertInstanceOf(
            DateRangeSetterInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testSetDateRange
     */
    public function testSetDateRange()
    {
        $this
            ->durationWeeksMock
            ->expects($this->once())
            ->method('setDateRange')
            ->with(
                $this->identicalTo($this->carbonFromMock),
                $this->identicalTo($this->carbonToMock)
            );

        $this
            ->pricingDaysMock
            ->expects($this->once())
            ->method('setDateRange')
            ->with(
                $this->identicalTo($this->carbonFromMock),
                $this->identicalTo($this->carbonToMock)
            );

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setDateRange($this->carbonFromMock, $this->carbonToMock)
        );
    }

    /**
     * testPriceReturnWeeksPriceAsIsLower
     */
    public function testPriceReturnWeeksPriceAsIsLower()
    {
        $duration = 2;
        $rate = 8;
        $weekPrice = $duration * $rate;
        $dayPrice = $weekPrice + 1;

        $this
            ->pricingDaysMock
            ->expects($this->once())
            ->method('price')
            ->willReturn($dayPrice);

        $this
            ->durationWeeksMock
            ->expects($this->once())
            ->method('duration')
            ->willReturn($duration);
        
        $this
            ->ratesMock
            ->expects($this->once())
            ->method('rate')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn($rate);

        $this->assertEquals(
            $weekPrice,
            $this->objectUnderTest->price()
        );
    }

    /**
     * testPriceReturnDaysPriceAsIsLower
     */
    public function testPriceReturnDaysPriceAsIsLower()
    {
        $duration = 2;
        $rate = 8;
        $weekPrice = $duration * $rate;
        $dayPrice = $weekPrice - 1;

        $this
            ->pricingDaysMock
            ->expects($this->once())
            ->method('price')
            ->willReturn($dayPrice);

        $this
            ->durationWeeksMock
            ->expects($this->once())
            ->method('duration')
            ->willReturn($duration);
        
        $this
            ->ratesMock
            ->expects($this->once())
            ->method('rate')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn($rate);

        $this->assertEquals(
            $dayPrice,
            $this->objectUnderTest->price()
        );
    }
}
