<?php

namespace JustPark\Test\Pricing;

use Carbon\Carbon;
use JustPark\Duration\Hours as DurationHours;
use JustPark\Pricing\Hours;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\Pricing\Rates;

class HoursTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Pricing Hours instance.
     *
     * @var JustPark\Pricing\Hours
     */
    private $objectUnderTest;

    /** @var Rates */
    private $ratesMock;

    /** @var DurationHours */
    private $durationHoursMock;

    /** @var Carbon */
    private $carbonFromMock;

    /** @var Carbon */
    private $carbonToMock;

    /**
     * Instantiate the Hours class using the Laravel IoC container.
     */
    public function setUp()
    {
        $this->ratesMock = $this->createMock(Rates::class);
        $this->durationHoursMock = $this->createMock(DurationHours::class);

        $this->carbonFromMock = $this->createMock(Carbon::class);
        $this->carbonToMock = $this->createMock(Carbon::class);

        $this->objectUnderTest = new Hours(
            $this->ratesMock,
            $this->durationHoursMock
        );
    }

    /**
     * testImplementsPriceCalcInterface
     */
    public function testImplementsPriceCalcInterface()
    {
        $this->assertInstanceOf(
            PriceCalcInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testSetDateRange
     */
    public function testSetDateRange()
    {
        $this
            ->durationHoursMock
            ->expects($this->once())
            ->method('setDateRange')
            ->with(
                $this->identicalTo($this->carbonFromMock),
                $this->identicalTo($this->carbonToMock)
            );

        $this->objectUnderTest->setDateRange(
                $this->carbonFromMock,
                $this->carbonToMock
        );
    }

    /**
     * testSetDateRangeReturn
     */
    public function testSetDateRangeReturn()
    {
        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setDateRange(
                    $this->carbonFromMock,
                    $this->carbonToMock
            )
        );
    }

    /**
     * testPrice
     */
    public function testPrice()
    {
        $duration = 7;
        $rate = 5;

        $this
            ->durationHoursMock
            ->expects($this->once())
            ->method('duration')
            ->willReturn($duration);

        $this
            ->ratesMock
            ->expects($this->once())
            ->method('rate')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn($rate);

        $this->assertEquals(
            $rate * $duration,
            $this->objectUnderTest->price()
        );
    }
}
