<?php

namespace JustPark\Test\Pricing;

use Carbon\Carbon;
use JustPark\Duration\Days as DurationDays;
use JustPark\Pricing\Days;
use JustPark\Pricing\Hours;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\DateRangeSetterInterface;
use JustPark\Pricing\Rates;

class DaysTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Pricing Days instance.
     *
     * @var JustPark\Pricing\Days
     */
    private $objectUnderTest;

    /** @var Rates */
    private $ratesMock;

    /** @var Days */
    private $durationDaysMock;

    /** @var Hours */
    private $hoursMock;

    /** @var Carbon */
    private $carbonFromMock;

    /** @var Carbon */
    private $carbonToMock;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->ratesMock = $this->createMock(Rates::class);
        $this->durationDaysMock = $this->createMock(DurationDays::class);
        $this->hoursMock = $this->createMock(Hours::class);

        $this->carbonFromMock = $this->createMock(Carbon::class);
        $this->carbonToMock = $this->createMock(Carbon::class);
        
        $this->objectUnderTest = new Days(
                $this->ratesMock,
                $this->durationDaysMock,
                $this->hoursMock
        );
    }

    /**
     * testImplementsPriceCalcInterface
     */
    public function testImplementsPriceCalcInterface()
    {
        $this->assertInstanceOf(
            PriceCalcInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testImplementsDateRangeSetterInterface
     */
    public function testImplementsDateRangeSetterInterface()
    {
        $this->assertInstanceOf(
            DateRangeSetterInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testSetDateRange
     */
    public function testSetDateRange()
    {
        $this
            ->durationDaysMock
            ->expects($this->once())
            ->method('setDateRange')
            ->with(
                $this->identicalTo($this->carbonFromMock),
                $this->identicalTo($this->carbonToMock)
            );

        $this
            ->hoursMock
            ->expects($this->once())
            ->method('setDateRange')
            ->with(
                $this->identicalTo($this->carbonFromMock),
                $this->identicalTo($this->carbonToMock)
            );

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setDateRange($this->carbonFromMock, $this->carbonToMock)
        );
    }

    /**
     * testPriceReturnDaysPriceAsIsLower
     */
    public function testPriceReturnDaysPriceAsIsLower()
    {
        $duration = 2;
        $rate = 8;
        $dayPrice = $duration * $rate;
        $hoursPrice = $dayPrice + 1;

        $this
            ->hoursMock
            ->expects($this->once())
            ->method('price')
            ->willReturn($hoursPrice);

        $this->objectUnderTest->setDateRange($this->carbonFromMock, $this->carbonToMock);

        $this
            ->durationDaysMock
            ->expects($this->once())
            ->method('duration')
            ->willReturn($duration);
        
        $this
            ->ratesMock
            ->expects($this->once())
            ->method('rate')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn($rate);

        $this->assertEquals(
            $dayPrice,
            $this->objectUnderTest->price()
        );
    }

    /**
     * testPriceReturnHoursPriceAsIsLower
     */
    public function testPriceReturnHoursPriceAsIsLower()
    {
        $duration = 2;
        $rate = 8;
        $dayPrice = $duration * $rate;
        $hoursPrice = $dayPrice - 1;

        $this->objectUnderTest->setDateRange($this->carbonFromMock, $this->carbonToMock);

        $this
            ->hoursMock
            ->expects($this->once())
            ->method('price')
            ->willReturn($hoursPrice);

        $this
            ->durationDaysMock
            ->expects($this->once())
            ->method('duration')
            ->willReturn($dayPrice);

        $this
            ->ratesMock
            ->expects($this->once())
            ->method('rate')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn($rate);

        $this->assertEquals(
            $hoursPrice,
            $this->objectUnderTest->price()
        );
    }
}
