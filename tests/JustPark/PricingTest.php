<?php

namespace JustPark\Test;

use Carbon\Carbon;
use JustPark\Pricing;
use JustPark\Pricing\Months as PricingMonths;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\DateRangeSetterInterface;

class PricingTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Pricing Days instance.
     *
     * @var JustPark\Pricing\Days
     */
    private $objectUnderTest;

    /** @var PricingMonths */
    private $pricingMonthsMock;

    /** @var Carbon */
    private $carbonFromMock;

    /** @var Carbon */
    private $carbonToMock;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->pricingMonthsMock = $this->createMock(PricingMonths::class);

        $this->carbonFromMock = $this->createMock(Carbon::class);
        $this->carbonToMock = $this->createMock(Carbon::class);
        
        $this->objectUnderTest = new Pricing($this->pricingMonthsMock);
    }

    /**
     * testImplementsPriceCalcInterface
     */
    public function testImplementsPriceCalcInterface()
    {
        $this->assertInstanceOf(
            PriceCalcInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testImplementsDateRangeSetterInterface
     */
    public function testImplementsDateRangeSetterInterface()
    {
        $this->assertInstanceOf(
            DateRangeSetterInterface::class,
            $this->objectUnderTest
        );
    }

    /**
     * testSetDateRange
     */
    public function testSetDateRange()
    {
        $this
            ->pricingMonthsMock
            ->expects($this->once())
            ->method('setDateRange')
            ->with(
                $this->identicalTo($this->carbonFromMock),
                $this->identicalTo($this->carbonToMock)
            );

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setDateRange($this->carbonFromMock, $this->carbonToMock)
        );
    }

    /**
     * testPrice
     */
    public function testPrice()
    {
        $expectedPrice = 21;
        $this
            ->pricingMonthsMock
            ->expects($this->once())
            ->method('price')
            ->willReturn($expectedPrice);

        $this->assertEquals(
            $expectedPrice,
            $this->objectUnderTest->price()
        );
    }
}
