![JustPark](http://i.imgur.com/ULvqtnl.png)

# JustPark Pricing Test

The challenge is to implement a class (or component) that will take an array of [Carbon](https://github.com/briannesbitt/Carbon) date instances for the time that a parking space should be booked in the following format...

```php
$input = [
    [new Carbon(..start..), new Carbon(..end..)],
    [new Carbon(..start..), new Carbon(..end..)],
    [new Carbon(..start..), new Carbon(..end..)]
];
```

... and applying a number of duration pricing rules to return a price for the available date range(s).


## Setup

A base framework has been provided as a starting point. This framework includes the [Laravel IoC container](http://laravel.com/docs/ioc), a `PricingCalculator` class and a [PHPUnit](http://phpunit.de/) test file that can be used to test the class functionality. To setup the base project:

1. Install PHP7
2. Install [composer](https://getcomposer.org/)
3. `composer install`

To run the tests:

```
composer test
```

## The Rules

| # | Rule |
| --- | --- |
| 1 | If a booking last longer than 24 hours, we no longer calculate prices in terms of hours parked. |
| 2 | If booking spans multiple days and ends before 5am, then the final day is not included in the calculation. |
| 3 | The monthly rate is used where the weekly and daily rate is more expensive. |
| 4 | The weekly rate is used where the daily rate is more expensive. |
| 5 | The daily rate is used where the hourly rate is more expensive. |

## The Pricing

| Hourly | Daily | Weekly | Monthly |
| --- | --- | --- | --- |
| £2 | £5 | £20 | £70 |

## The Examples

| # | Start | End | Duration | Cost | Notes |
| --- | --- | --- | --- | --- | --- |
| 1 | 24th Jan, 14:00 | 25th Jan, 03:00 | 1 day | £5 | Finishes before 5am. |
| 2 | 24th Jan, 14:00 | 25th Jan, 12:00 | 2 days | £10 | Finishes after 5am, so 2 days despite it being less than 24 hours. |
| 3 | 24th Jan, 14:00 | 18th Feb, 15:00 | 3 weeks and 5 days | £70 | Monthly price (£70) is less than (3 (weeks) * £20 + 5 (days) * £5) and (4 (weeks) * £20). |
