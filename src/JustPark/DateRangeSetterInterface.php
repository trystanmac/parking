<?php

namespace JustPark;

use Carbon\Carbon;

/**
 * DateRangeSetterInterface
 */
interface DateRangeSetterInterface 
{
    /**
     * setDateRange
     * 
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end);
}
