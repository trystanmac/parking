<?php

namespace JustPark\Pricing;

use Carbon\Carbon;
use JustPark\DateRangeSetterInterface;
use JustPark\Duration\Days as DurationDays;
use JustPark\Pricing\Hours as PricingHours;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\Pricing\Rates;

/**
 * Class Pricing Days
 */
class Days implements PriceCalcInterface, DateRangeSetterInterface
{
    /** @var Rates */
    protected $rates;

    /** @var PricingHours */
    protected $pricingHours;

    /** @var DurationDays */
    protected $durationDays;

    /**
     * constructor
     *
     * @var Rates $rates
     * @var DurationDays $durationDays
     * @var PricingHours $pricingHours
     */
    public function __construct(
        Rates $rates,
        DurationDays $durationDays,
        PricingHours $pricingHours
    ) {
        $this->rates = $rates;
        $this->durationDays = $durationDays;
        $this->pricingHours = $pricingHours;
    }

    /**
     * setDateRange
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end)
    {
        $this->durationDays->setDateRange($start, $end);
        $this->pricingHours->setDateRange($start, $end);

        return $this;
    }

    /**
     * price
     *
     * @return float
     */
    public function price(): float
    {
        return min([
            $this->pricingHours->price(),
            $this->rates->rate($this) * $this->durationDays->duration(),
        ]);
    }
}
