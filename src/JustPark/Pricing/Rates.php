<?php

namespace JustPark\Pricing;

use Exception;
use JustPark\Pricing\Days as PricingDays;
use JustPark\Pricing\Hours as PricingHours;
use JustPark\Pricing\Months as PricingMonths;
use JustPark\Pricing\Weeks as PricingWeeks;
use JustPark\Pricing\PriceCalcInterface;

/**
 * Class Pricing Rates
 */
class Rates
{
    protected $rates = [];

    /**
     * Rate
     *
     * @param PriceCalcInterface $priceCalc
     * @return int Rate
     */
    public function rate(PriceCalcInterface $priceCalc): int
    {
        $className = get_class($priceCalc);
        $this->throwExceptionIfRateDoesNotExist($className);
        return $this->rates[$className];
    }

    /**
     * add
     *
     * @param PriceCalcInterface $priceCalc
     * @param int $rate
     * @return Rates $this
     */
    public function add(PriceCalcInterface $priceCalc, int $rate)
    {
        $className = get_class($priceCalc);
        $this->rates[$className] = $rate;
        return $this;
    }

    protected function throwExceptionIfRateDoesNotExist($className)
    {
        if (!isset($this->rates[$className])) {
            throw new Exception(
                "Error, no rates available for invalid class '{$className}'"
            );
        }
    }
}
