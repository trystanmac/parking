<?php

namespace JustPark\Pricing;

use Carbon\Carbon;
use JustPark\DateRangeSetterInterface;
use JustPark\Duration\Weeks as DurationWeeks;
use JustPark\Pricing\Days as PricingDays;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\Pricing\Rates;

/**
 * Class Pricing Weeks
 */
class Weeks implements PriceCalcInterface, DateRangeSetterInterface
{
    /** @var Rates */
    protected $rates;

    /** @var PricingDays */
    protected $pricingDays;

    /** @var DurationWeeks */
    protected $durationWeeks;

    /**
     * constructor
     *
     * @var Rates $rates
     * @var DurationWeeks $durationDays
     * @var PricingDays $pricingDays
     */
    public function __construct(
        Rates $rates,
        DurationWeeks $durationWeeks,
        PricingDays $pricingDays
    ) {
        $this->rates = $rates;
        $this->durationWeeks = $durationWeeks;
        $this->pricingDays = $pricingDays;
    }

    /**
     * setDateRange
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end)
    {
        $this->durationWeeks->setDateRange($start, $end);
        $this->pricingDays->setDateRange($start, $end);

        return $this;
    }

    /**
     * price
     *
     * @return float
     */
    public function price(): float
    {
        return min([
            $this->pricingDays->price(),
            $this->durationWeeks->duration() * $this->rates->rate($this),
        ]);
    }
}
