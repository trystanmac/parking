<?php

namespace JustPark\Pricing;

use Carbon\Carbon;

/**
 * PriceCalcInterface
 */
interface PriceCalcInterface 
{
    /**
     * price
     * 
     * @return float
     */
    public function price(): float;
}
