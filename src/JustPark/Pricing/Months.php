<?php

namespace JustPark\Pricing;

use Carbon\Carbon;
use JustPark\DateRangeSetterInterface;
use JustPark\Duration\Months as DurationMonths;
use JustPark\Pricing\Weeks as PricingWeeks;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\Pricing\Rates;

/**
 * Class Pricing Months
 */
class Months implements PriceCalcInterface, DateRangeSetterInterface
{
    /** @var Rates */
    protected $rates;

    /** @var PricingWeeks */
    protected $pricingWeeks;

    /** @var DurationMonths */
    protected $durationMonths;

    /**
     * constructor
     *
     * @var Rates $rates
     * @var DurationMonths $durationMonths
     * @var PricingWeeks $pricingWeeks
     */
    public function __construct(
        Rates $rates,
        DurationMonths $durationMonths,
        PricingWeeks $pricingWeeks
    ) {
        $this->rates = $rates;
        $this->durationMonths = $durationMonths;
        $this->pricingWeeks = $pricingWeeks;
    }

    /**
     * setDateRange
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end)
    {
        $this->durationMonths->setDateRange($start, $end);
        $this->pricingWeeks->setDateRange($start, $end);
        return $this;
    }

    /**
     * price
     *
     * @return float
     */
    public function price(): float
    {
        return min([
            $this->pricingWeeks->price(),
            $this->durationMonths->duration() * $this->rates->rate($this),
        ]);
    }
}
