<?php

namespace JustPark\Pricing;

use Carbon\Carbon;

use JustPark\Duration\Hours as DurationHours;
use JustPark\Pricing\PriceCalcInterface;
use JustPark\Pricing\Rates;

/**
 * Class Pricing Hours
 */
class Hours implements PriceCalcInterface
{
    /** @var Rates */
    protected $rates;

    /** @var DurationHours */
    protected $durationHours;

    /**
     * Constructor
     *
     * @param \JustPark\Pricing\Rates $rates
     * @param \JustPark\Duration\Hours $durationHours
     */
    public function __construct(Rates $rates, DurationHours $durationHours)
    {
        $this->rates = $rates;
        $this->durationHours = $durationHours;
    }

    /**
     * setDateRange
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end)
    {
        $this->durationHours->setDateRange($start, $end);
        return $this;
    }

    /**
     * price
     *
     * @return float
     */
    public function price(): float
    {
        return $this->durationHours->duration() * $this->rates->rate($this);
    }
}
