<?php

namespace JustPark\Duration;

use Carbon\Carbon;

/**
 * DurationInterface
 */
interface DurationInterface 
{
    /**
     * setDateRange
     * 
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end);
}
