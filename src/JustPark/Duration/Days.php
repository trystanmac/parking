<?php

namespace JustPark\Duration;

use Carbon\Carbon;
use Exception;
use JustPark\DateRangeSetterInterface;
use JustPark\Duration\DurationInterface;
use JustPark\Pricing\Hours;
use JustPark\Pricing\Rates;

/**
 * Class Duration Days
 */
class Days implements DurationInterface, DateRangeSetterInterface
{
    /** @var Carbon */
    protected $start;

    /** @var Carbon */
    protected $end;

    /**
     * setDateRange
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end)
    {
        $this->throwExceptionIfEndBeforeStart($start, $end);
        $this->start = $start;
        $this->end = $end;

        return $this;
    }

    /**
     * duration
     *
     * @return int
     */
    public function duration(): int
    {
        $this->throwExceptionIfDateRangeNotSet();

        $duration = $this->getDaysSpan();
        
        $minumDayDuration = 1;
        if ($this->endTimeLessThanFiveAm() and $duration > $minumDayDuration) {
            // Note: We don't want number of days to reach Zero.
            //       The lowest it should reach is One, so that
            //       the daily charge will have a minimum value of one
            //       unit charge.
            //       The pricing logic will give the lowest of the Hourly or Daily
            //       charge.
            $duration--;
        }

        return $duration;
    }

    protected function throwExceptionIfEndBeforeStart(Carbon $start, Carbon $end)
    {
        if ($end->lt($start)) {
            throw new Exception('Error, End date should be after Start date.');
        }
    }

    protected function throwExceptionIfDateRangeNotSet()
    {
        if (null === $this->end) {
            throw new Exception('Error, DateRange not set.');
        }
    }

    protected function getDaysSpan()
    {
        $start = clone $this->start;
        $end = clone $this->end;

        $end->addDays(1)->startOfDay();
        $start->startOfDay();

        return $start->diffInDays($end);
    }

    protected function endTimeLessThanFiveAm(): bool
    {
        // Maybe the time set at 5am could be configurable or
        // taken from the database.
        $carbonFiveAmCheck = (new Carbon)->setTime(05, 00, 00);
        $carbonEndTime = (new Carbon)->setTime($this->end->hour, $this->end->minute, $this->end->second);

        return $carbonEndTime->lt($carbonFiveAmCheck);
    }
}
