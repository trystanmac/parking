<?php

namespace JustPark\Duration;

use Carbon\Carbon;
use Exception;
use JustPark\DateRangeSetterInterface;
use JustPark\Duration\DurationInterface;

/**
 * Class Duration Weeks
 */
class Weeks implements DurationInterface, DateRangeSetterInterface
{
    /** @var Carbon */
    protected $start;

    /** @var Carbon */
    protected $end;

    /**
     * setDateRange
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end)
    {
        $this->throwExceptionIfEndBeforeStart($start, $end);
        $this->start = $start;
        $this->end = $end;
        return $this;
    }

    /**
     * duration
     *
     * @return int
     */
    public function duration(): int
    {
        $this->throwExceptionIfDateRangeNotSet();

        return $this->end->diffInWeeks($this->start) + 1;
    }

    protected function throwExceptionIfEndBeforeStart(Carbon $start, Carbon $end)
    {
        if ($end->lt($start)) {
            throw new Exception('Error, End date should be after Start date.');
        }
    }

    protected function throwExceptionIfDateRangeNotSet()
    {
        if (null === $this->end) {
            throw new Exception('Error, DateRange not set.');
        }
    }
}
