<?php

namespace JustPark\Duration;

use Carbon\Carbon;
use Exception;
use JustPark\Duration\DurationInterface;
use JustPark\DateRangeSetterInterface;

/**
 * Class Duration Hours
 */
class Hours implements DurationInterface, DateRangeSetterInterface
{
    /** @var Carbon */
    protected $start;

    /** @var Carbon */
    protected $end;

    protected $minutesInHour = 60;

    /**
     * setDateRange
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end)
    {
        $this->throwExceptionIfEndBeforeStart($start, $end);
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * duration
     *
     * @return float
     */
    public function duration(): int
    {
        $this->throwExceptionIfDateRangeNotSet();

        return ceil($this->end->diffInMinutes($this->start) / $this->minutesInHour);
    }

    protected function throwExceptionIfEndBeforeStart(Carbon $start, Carbon $end)
    {
        if ($end->lt($start)) {
            throw new Exception('Error, End date should be after Start date.');
        }
    }

    protected function throwExceptionIfDateRangeNotSet()
    {
        if (null === $this->end) {
            throw new Exception('Error, DateRange not set.');
        }
    }
}
