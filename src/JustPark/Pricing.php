<?php

namespace JustPark;

use Carbon\Carbon;
use JustPark\DateRangeSetterInterface;
use JustPark\Pricing\Months as PricingMonths;
use JustPark\Pricing\PriceCalcInterface;

/**
 * Class Pricing
 */
class Pricing implements PriceCalcInterface, DateRangeSetterInterface
{
    /** @var PricingMonths */
    protected $pricingMonths;

    /**
     * constructor
     *
     * @var PricingMonths $durationDays
     */
    public function __construct(
        PricingMonths $pricingMonths
    ) {
        $this->pricingMonths = $pricingMonths;
    }

    /**
     * setDateRange
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function setDateRange(Carbon $start, Carbon $end)
    {
        $this->pricingMonths->setDateRange($start, $end);
        return $this;
    }

    /**
     * price
     *
     * @return float
     */
    public function price(): float
    {
        return $this->pricingMonths->price();
    }
}
