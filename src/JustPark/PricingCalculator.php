<?php

namespace JustPark;

use Illuminate\Container\Container;
use JustPark\Pricing\Days;
use JustPark\Pricing\Hours;
use JustPark\Pricing\Months;
use JustPark\Pricing\Weeks;
use JustPark\Pricing\Rates;

/**
 * Class PricingCaluclator
 */
class PricingCalculator
{
    /** @var Container */
    protected $container;

    /**
     * Calculate a price based upon an array of start and
     * end date pairs.
     *
     * @param  array  $periods
     * @return float
     */
    public function calculate(array $periods)
    {
        $this->container = new Container;

        $this->initialiseRates();

        $pricing = $this->getPricing();

        $price = 0;

        foreach ($periods as $period) {
            $pricing->setDateRange($period[0], $period[1]);
            $price = $price + $pricing->price();
        }

        return $price;
    }

    protected function getPricing()
    {
        return $this->container->make(Pricing::class);
    }

    protected function initialiseRates()
    {
        $this
            ->container
            ->singleton(Rates::class);

        $this
            ->container
            ->make(Rates::class)
            ->add($this->container->make(Hours::class), 2)
            ->add($this->container->make(Days::class), 5)
            ->add($this->container->make(Weeks::class), 20)
            ->add($this->container->make(Months::class), 70);
    }
}
